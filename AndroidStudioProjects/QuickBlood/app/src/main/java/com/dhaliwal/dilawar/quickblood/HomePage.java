package com.dhaliwal.dilawar.quickblood;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomePage extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        final Button needBloodButton=(Button)findViewById(R.id.needBloodButton);
        needBloodButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                Intent intent=new Intent(HomePage.this,SearchDonor.class);
                startActivity(intent);
            }
        });

        final Button homeLoginButton=(Button) findViewById(R.id.homeLoginButton);
        homeLoginButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                Intent i=new Intent(HomePage.this,LoginDonor.class);
                startActivity(i);
            }
        });

        final Button registerDonor=(Button)findViewById(R.id.registerButton);
        registerDonor.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                Intent i=new Intent(HomePage.this,DonorRegistration.class);
                startActivity(i);
            }
        });

    }
}
