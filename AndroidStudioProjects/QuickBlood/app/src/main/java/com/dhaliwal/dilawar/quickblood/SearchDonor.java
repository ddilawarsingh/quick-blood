package com.dhaliwal.dilawar.quickblood;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SearchDonor extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_donor);

        final Button searchButton=(Button)findViewById(R.id.searchDonorButton);
        searchButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                Intent intent=new Intent(SearchDonor.this,DonorList.class);
                startActivity(intent);
            }
        });
    }
}
