package com.dhaliwal.dilawar.quickblood;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DonorRegistration extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donor_registration);

        final Button nextDonor=(Button)findViewById(R.id.nextButton);
        nextDonor.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                Intent i=new Intent(DonorRegistration.this,CompleteRegistration.class);
                startActivity(i);
            }
        });

    }
}
