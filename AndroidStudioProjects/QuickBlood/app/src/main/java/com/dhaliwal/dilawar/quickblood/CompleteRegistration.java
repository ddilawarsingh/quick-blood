package com.dhaliwal.dilawar.quickblood;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class CompleteRegistration extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_registration);

        final Button compReg=(android.widget.Button)findViewById(R.id.compRegButton);
        compReg.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                Toast.makeText(CompleteRegistration.this,R.string.complete_registration,Toast.LENGTH_LONG).show();
            }
        });
    }
}
