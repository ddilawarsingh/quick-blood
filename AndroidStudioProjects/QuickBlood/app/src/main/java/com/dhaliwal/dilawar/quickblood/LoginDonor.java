package com.dhaliwal.dilawar.quickblood;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LoginDonor extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_donor);

        final Button generateOTP=(Button)findViewById(R.id.generateOtpButton);
        generateOTP.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                final TextView showOtpText=(TextView) findViewById(R.id.otpText2);
                final TextView showOtpEnterText=(TextView)findViewById(R.id.compOtpEnterText);
                final Button loginButton=(Button)findViewById(R.id.logInButton);

                showOtpText.setVisibility(View.VISIBLE);
                showOtpEnterText.setVisibility(View.VISIBLE);
                loginButton.setVisibility(View.VISIBLE);
            }
        });

    }


}
